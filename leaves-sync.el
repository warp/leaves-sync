;;; leaves-sync.el --- Sync `leaves-mode' buffers to source media -*- lexical-binding: t -*-

;;; Code:

(require 'leaves)
(require 'map)

(defvar leaves-sync-iface nil)
(defvar leaves-sync-env nil)
(defvar leaves-sync-st nil)
(defvar leaves-sync-frozen-p nil)

(defconst leaves-sync-process-buffer "Leaves Process"
  "Buffer for reporting on leaves-sync processes.")

(defcustom leaves-sync-consts
  '(("pdf-tools" exts ("pdf"))
    ("emms" exts ("mp3" "mp4" "m4a" "wav"))
    ("emms" process-p t)
    ("emms" time-based-p t)
    ("emms" nudge-factor .1))
  "Alist of constant values associated with syncable media formats."
  :type
  '(alist
    :key-type (string :tag "Name")
    :value-type (group (symbol :tag "Variable") sexp)))

(defcustom leaves-sync-vars
  '(("emms" paused-p emms-player-paused-p))
  "Alist of mutable variables associated with syncable media formats."
  :type
  '(alist
    :key-type (string :tag "Name")
    :value-type (group (symbol :tag "Request") symbol)))

(defcustom leaves-sync-fns
  '(("pdf-tools" add find-file-other-window)
    ("pdf-tools" leaf->pos 1+)
    ("pdf-tools" goto pdf-view-goto-page)
    ("pdf-tools" skip pdf-view-next-page)
    ("pdf-tools" nudge leaves--pdf-tools-nudge)
    ("emms" add emms-play-file)
    ("emms" goto leaves--emms-seek-to)
    ("emms" leaf->pos (lambda (leaf) (* leaf 30)))
    ("emms" skip emms-seek)
    ("emms" nudge emms-seek)
    ("emms" pause emms-pause)
    ("emms" play emms-pause)
    ("emms" stop emms-stop))
  "Alist of functions associated with syncable media formats.

Elements are of the form (IFACE ACTION FN), where IFACE is a
string naming an interface and (ACTION . FN) maps
`leaves-sync-mode' actions to format-specific functions,
described below.

add - Add a new source file FILE

leaf->pos - Return a position corresponding to leaf number LEAF

goto - Jump to an absolute position POS

skip - Jump forward or backward one leaf

nudge - Jump forward a fixed amount DELTA

play - Resume media playback

pause - Pause media playback

stop - End media playback"
  :type
  '(alist
    :key-type (string :tag "Name")
    :value-type (group (symbol :tag "Action") function)))

;;;###autoload
(define-minor-mode leaves-sync-mode
  "Mode for syncing `leaves-mode' buffers to source files."
  :init-value nil
  :lighter " Src"
  :keymap nil
  (if leaves-sync-mode
      (condition-case err (leaves--sync-init)
        (error
         (setq leaves-sync-mode nil)
         (message "%s" (error-message-string err))))
    (leaves--sync-quit)))

(defun leaves--sync-init ()
  "Load a source file for the first time.
Called when `leaves-sync-mode' is enabled."
  (unless (leaves-live-p)
    (error "Enable `leaves-mode' first"))
  (call-interactively #'leaves--sync-load-env)
  (let ((file (leaves--sync-find-file)))
    (unless file
      (error "No source file"))
    (leaves-sync-run 'add file))
  (setq leaves-sync-buffer (current-buffer))
  (unless (leaves--sync-init-p)
    (leaves--sync-quit)
    (error "Cancelled during process load"))
  (leaves-sync-pause)
  (pop-to-buffer leaves-buffer)
  (leaves-sync-pos))

(defun leaves--sync-quit ()
  "Close the loaded source file.
Called when `leaves-sync-mode' is disabled."
  (leaves-sync-run 'stop))

(defhydra hydra-leaves-sync nil "Sync"
  ("n" leaves-sync-forward "next")
  ("p" leaves-sync-backward "prev")
  ("g" leaves-sync-jump "goto")
  ("C-n" leaves-sync-nudge-forward "nudge forward")
  ("C-p" leaves-sync-nudge-backward "nudge backward")
  ("s" leaves-sync-toggle-paused "pause")
  ("f" leaves-sync-toggle-frozen-p "freeze")
  ("r" leaves-sync-pos "reset")
  ("m" hydra-leaves-motion/body "motion" :exit t))

; (define-key leaves-mode-map
;;   (kbd "C-c C-s") 'hydra-leaves-sync/body)

;; (define-key leaves-mode-map
;;   (kbd "C-[") 'leaves-sync-nudge-backward)
;; (define-key leaves-mode-map
;;   (kbd "C-]") 'leaves-sync-nudge-forward)
(define-key leaves-mode-map
  (kbd "C-}") 'leaves-sync-nudge-forward)
(define-key leaves-mode-map
  (kbd "C-{") 'leaves-sync-nudge-backward)
(define-key leaves-mode-map
  (kbd "C-'") 'leaves-sync-toggle-paused)

;;;###autoload
(defun leaves-sync-pos ()
  "Go to a location in the source file matching the current leaf."
  (interactive)
  (when leaves-leaf
    (leaves-sync-jump
     (leaves-sync-leaf->pos leaves-leaf))))

;;;###autoload
(defun leaves-sync-forward (&optional arg)
  (interactive "p")
  (leaves-sync-run-window 'skip
    (leaves-sync-leaf->pos arg)))

;;;###autoload
(defun leaves-sync-backward (&optional arg)
  (interactive "p")
  (leaves-sync-run-window 'skip
    (- (leaves-sync-leaf->pos arg))))

;;;###autoload
(defun leaves-sync-jump (pos)
  (interactive "nSource position: ")
  (leaves-sync-run-window 'goto pos))

;;;###autoload
(defun leaves-sync-nudge-forward (&optional arg)
  (interactive "p")
  (leaves-sync-run-window 'skip
    (* (leaves-sync-leaf->pos arg)
       (leaves-sync-ask 'nudge-factor))))

;;;###autoload
(defun leaves-sync-nudge-backward (&optional arg)
  (interactive "p")
  (leaves-sync-run-window 'skip
    (- (* (leaves-sync-leaf->pos arg)
          (leaves-sync-ask 'nudge-factor)))))

;;;###autoload
(defun leaves-sync-pause ()
  (interactive)
  (when (leaves-sync-ask 'time-based-p)
    (leaves-sync-run-window 'pause)
    (leaves-sync-nudge-backward)))

;;;###autoload
(defun leaves-sync-play ()
  (interactive)
  (when (leaves-sync-ask 'time-based-p)
    (leaves-sync-run-window 'play)))

;;;###autoload
(defun leaves-sync-toggle-paused (&optional arg)
  (interactive "P")
  (leaves--toggle arg (leaves-sync-get 'paused-p) #'leaves--sync-set-paused))

(defun leaves--sync-set-paused (active)
  (if active
      (leaves-sync-pause)
    (leaves-sync-play)))

;;;###autoload
(defun leaves-sync-toggle-frozen-p (&optional arg)
  "Toggle freezing of the synced media in place while navigating."
  (interactive "P")
  (leaves--toggle arg leaves-sync-frozen-p #'leaves--sync-set-frozen-p))

(defun leaves--sync-set-frozen-p (active)
  (if active
      (setq leaves-sync-frozen-p t)
    (setq leaves-sync-frozen-p nil)
    (leaves-sync-pos)))

(defun leaves-sync-leaf->pos (&optional leaf)
  (leaves-sync-run-window 'leaf->pos (or leaf 1)))

(defun leaves-sync-ask (sym)
  (car (map-elt leaves-sync-env sym)))

(defun leaves-sync-get (var)
  (symbol-value (leaves-sync-ask var)))

(defun leaves-sync-put (var value)
  (map-put leaves-sync-env var (list value)))

(defun leaves-sync-run (action &rest args)
  "Run a sync function mapped to ACTION."
  (let ((fn (leaves-sync-ask action)))
    (when (functionp fn)
      (apply fn args))))

(defun leaves-sync-run-window (action &rest args)
  "Run a sync function mapped to ACTION in the sync window.
If the source has no buffer associated with it, run in the
selected window."
  (with-selected-window
      (if (buffer-live-p leaves-sync-buffer)
          (or (get-buffer-window leaves-sync-buffer)
              (display-buffer leaves-sync-buffer))
        (selected-window))
    (apply #'leaves-sync-run (cons action args))))

(defun leaves--sync-find-file ()
  "Read a source file from the user.
Filter by file extensions supported by currently loaded
`leaves-sync-env'.

 Return nil if user quits or selects a directory."
  (let ((file
         (condition-case nil
             (read-file-name
              "Source file: " nil nil t nil #'leaves--sync-file-p)
           (quit nil))))
    (if (file-directory-p file) nil file)))

(defun leaves--sync-file-p (file)
  "Non-nil if `leaves-sync-env' supports FILE.
Always returns t on directories."
  (or (file-directory-p file)
      (member (file-name-extension file)
              (leaves-sync-ask 'exts))))

(defun leaves--sync-load-env-map (iface map)
  (map-values
   (map-filter
    (lambda (key val)
      (string= key iface))
    map)))

(defun leaves--sync-load-env (iface)
  (interactive
   (list
    (completing-read "Sync interface to load: "
      (leaves--sync-list-interfaces) nil t nil nil leaves-sync-iface)))
  (setq leaves-sync-iface iface)
  (setq leaves-sync-env
        (append (leaves--sync-load-env-map iface leaves-sync-consts)
                (leaves--sync-load-env-map iface leaves-sync-vars)
                (leaves--sync-load-env-map iface leaves-sync-fns))))

(defun leaves--sync-list-interfaces ()
  (delete-dups
   (map-keys
    (append leaves-sync-consts leaves-sync-vars leaves-sync-fns))))

(defun leaves--sync-init-p ()
  "Return t if the source interface was initialized, or nil.
If the interface uses an external process, prompt the user to
confirm it started."
  (if (null (leaves-sync-ask 'process-p)) t
    (let (init-p)
      (save-window-excursion
        (setq
         init-p
         (y-or-n-p
          ;; buffer necessary in case prompt is overwritten by process echo
          (with-output-to-temp-buffer leaves-sync-process-buffer
            (princ "Did the source process load OK? [y/n]")))))
      (kill-buffer leaves-sync-process-buffer)
      init-p)))

(defun leaves-sync-insert (begin end)
  (interactive "nStarting at source pos: \nnEnding at source pos: ")
  (let ((i begin))
    (while (< i end)
      (insert "\n")
      (setq i (+ i (leaves-sync-leaf->pos))))))

;; Interface-specific utility

(defun leaves--pdf-tools-nudge (delta)
  (if (< delta 0)
      (pdf-view-previous-line-or-previous-page (- delta))
    (pdf-view-next-line-or-next-page delta)))

(defun leaves--emms-seek-to (seconds)
  "Like `emms-seek-to' SECONDS, but exit gracefully on errors."
  (unless emms-player-playing-p
    (emms-start))
  (condition-case err (emms-seek-to seconds)
    (error
     (message (error-message-string err)))))

(provide 'leaves-sync)

;;; leaves-sync.el ends here

